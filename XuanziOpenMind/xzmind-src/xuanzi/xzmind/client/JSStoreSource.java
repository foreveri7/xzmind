package xuanzi.xzmind.client;

import sbaike.client.h5.client.Param;
import xuanzi.xzmind.core.StoreSource;

public class JSStoreSource implements StoreSource {

	Param param;
	
	public JSStoreSource(Param param) {
		this.param = param;
	}

	@Override
	public boolean isAutoSave() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void read(Object path, Result res) {
		if(param.has("markdown"))
			res.result(param.string("markdown"));
		else
			res.result("");
	}

	@Override
	public void save(Object path, String content, Result res) {
		call(param,content);
		res.result("ok");
	}

	private native void call(Param param2, String content) /*-{
		param2.onSave(content);
	}-*/;

	@Override
	public String getName(Object path) { 
		return param.has("title")?param.string("title"):param.string("path");
	}

}
